//@author: Robin Nieto
// Funcion que  setea background de los bloques  con imágenes, cambia las imágenes  de acuerdo el recorrido del scroll  de los contenidos especiales  
jQuery(document).ready(function(){
	var numero = jQuery(".fondo").size();
	var estilo1;
	var imgs = new Array();
	var colors = new Array();
	var tops = new Array();
	var view = new Array();
	var altura = 0;
	var alturaview = 0;

	for (var  i = 1;i <= numero;i++){
		var estilod = jQuery(".views-row-"+ i).find(".colorbg").html()+ "; " + jQuery(".views-row-"+ i).find(".colorfont").html(); ;
		estilo1 = jQuery(".views-row-"+ i).find(".imabg").html();
		
		if(estilo1 != null && estilo1 != ""){
			if(estilo1 == null)
				estilo1 = "";
			else
				estilo1 += "; background-size:100%; background-repeat:no-repeat; ";
			
			tops.push(altura);
			console.log(altura);
			imgs.push(estilo1);
			colors.push(estilod);
		}
		
		altura +=jQuery(".views-row-"+ i).height(); 
		view.push(alturaview);
		alturaview +=jQuery(".views-row-"+ i).height();
	}
	
	jQuery(".view-especial").attr('style', imgs[0]);
	jQuery(".view-content").attr('style', colors[0]);
	jQuery(".views-row-1").addClass("activa");
	
	var n_view =0;
	
	jQuery(window).scroll(function(){
		var im = '';
		var cl = '';

		jQuery(tops).each (function(index,item) {
			if(jQuery(window).scrollTop() >= item){
				jQuery(".views-row-" +index).addClass("activa");
				jQuery(".views-row-" + (index-1)).removeClass("activa");
				im = imgs[index] + (index != 0 ? " background-position:center;" : "");
				cl = colors[index];
			}
		});
		
		jQuery(view).each (function(index,item) {
			if(jQuery(window).scrollTop() >= item){
				n_view = index +1;
			}
		});
	 
	 	jQuery(".view-especial").attr('style', im + "background-position-y:" + jQuery(window).scrollTop() + "px;");
		jQuery(".view-content").attr('style', cl);
		jQuery(".views-row").removeClass("activa");
		jQuery(".views-row-"+n_view).addClass("activa");
		console.log(n_view);
		
		
	  });
});