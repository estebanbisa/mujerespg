<?php

/**
 * @file
 * Describe comments entity admin form.
 */

/**
 * Comments entity admin form.
 *
 * @param array $form
 * @param $form_state
 * @return array
 */
function comments_entity_admin_form($form = array(), $form_state, $type) {

  $settings = comments_entity_settings_get($type);

  $form['settings'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['settings']['strategy'] = array(
    '#title' => t('Strategy'),
    '#description' => t('Whitelist or blacklist entities on which comments entities are enabled.'),
    '#type' => 'select',
    '#options' => array(
      'whitelist' => t('Whitelist'),
      'blacklist' => t('Blacklist'),
    ),
    '#default_value' => $settings['strategy'],
  );

  $entities = entity_get_info();

  $entity_options = array();

  foreach($entities as $key => $entity_info) {
    $entity_options[$key] = $entity_info['label'];
  }

  $form['settings']['entities'] = array(
    '#title' => t('Entities with comments_entity enabled.'),
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => $entity_options,
    '#ajax' => array(
      'callback' => 'comments_entity_entity_choice_js_callback',
      'wrapper' => 'comments-entity-bundles',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#default_value' => $settings['entities'],
  );

  $form['settings']['bundles'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#attributes' => array(
      'id' => 'comments-entity-bundles',
    )
  );

  $entities_selected = isset($form_state['values']['settings']['entities']) ?
    (array) $form_state['values']['settings']['entities'] : $settings['entities'];

  if($entities_selected) {
    foreach($entities_selected as $entity) {

      // Only if entity has bundles defined and
      // is not by itself the only bundle.
      if(!empty($entities[$entity]['bundles']) &&
        (count($entities[$entity]['bundles']) > 0 &&
          !isset($entities[$entity]['bundles'][$entity]))) {
        // Initialize the fieldset for the entity type bundles
        $form['settings']['bundles'][$entity] = array(
          '#title' => t('Bundles of @entity_type_label',
            array(
              '@entity_type_label' => $entities[$entity]['label'],
            )
          ),
          '#type' => 'fieldset',
        );

        foreach($entities[$entity]['bundles'] as $bundle_key => $bundle_info) {

          $form['settings']['bundles'][$entity][$bundle_key] = array(
            '#title' => $bundle_info['label'],
            '#type' => 'checkbox',
            '#default_value' =>
              isset($settings['bundles'][$entity][$bundle_key]) ?
                $settings['bundles'][$entity][$bundle_key] : 0,
          );

        }

      }
    }
  }

  $form['settings']['display_comment_form'] = array(
    '#title' => t('Display uppermost comment form'),
    '#type' => 'select',
    '#options' => array(
      'show' => t('Always'),
      'hide' => t('Hide initially'),
    ),
    '#default_value' => 'hide',
  );

  $form['settings']['approval'] = array(
    '#title' => t('Display uppermost comment form'),
    '#type' => 'select',
    '#options' => array(
      1 => t('New comments_entity are considered approved.'),
      0 => t('New comments_entity are subject to approval.')
    ),
    '#default_value' => 'hide',
  );

  $form['actions']['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Ajax callback
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function comments_entity_entity_choice_js_callback($form, $form_state) {
  return $form['settings']['bundles'];
}

/**
 * Comments Entity Admin form validation
 *
 * @param $form
 * @param $form_state
 */
function comments_entity_admin_form_validate($form, $form_state) {
  // Only defined because it is called by ajax system api.
}

/**
 * Comments Entity Admin form submission
 *
 * @param $form
 * @param $form_state
 */
function comments_entity_admin_form_submit($form, $form_state) {
  $type = $form_state['build_info']['args'][0];
  $settings = $form_state['values']['settings'];
  comments_entity_settings_save($settings, $type);
  drupal_set_message(t('Comment settings updated.'));
}
