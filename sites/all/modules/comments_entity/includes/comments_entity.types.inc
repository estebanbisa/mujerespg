<?php

/**
 * @file
 * Describe comments entity types functions.
 */

/**
 * Menu callback: display an overview of available types.
 */
function comments_entity_types_overview() {

  $header = array(
    t('Name'),
    t('Operations'),
  );

  $rows = array();

  // Loop through all defined comments entity types.
  foreach (comments_entity_types() as $type => $comments_entity_type) {

    // Build the operation links for the current comments entity type.
    $links = menu_contextual_links('comments-entity', 'admin/structure/comments_entity/types', array(strtr($type, array('_' => '-'))));

    // Add the profile type's row to the table's rows array.
    $rows[] = array(
      theme('comments_entity_type_admin_overview', array('comments_entity_type' => $comments_entity_type)),
      theme('links', array('links' => $links, 'attributes' => array('class' => 'links inline operations'))),
    );
  }

  // If no comments entity types are defined...
  if (empty($rows)) {
    // Add a standard empty row.
    $rows[] = array(
      array(
        'data' => t('There are no comments entity types defined on this site.'),
        'colspan' => 2,
      )
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Builds an overview of a comments entity type for display to an administrator.
 *
 * @param $variables
 *   An array of variables used to generate the display; by default includes the
 *     type key with a value of the comments entity type object.
 *
 * @ingroup themeable
 */
function theme_comments_entity_type_admin_overview($variables) {
  $comments_entity_type = $variables['comments_entity_type'];

  $output = check_plain($comments_entity_type['name']);
  $output .= ' <small>' . t('(Machine name: @type)', array('@type' => $comments_entity_type['type'])) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($comments_entity_type['description']) . '</div>';

  return $output;
}