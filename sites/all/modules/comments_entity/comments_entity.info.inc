<?php

/**
 * @file
 * Describe entity properties.
 */

/**
 * Implements hook_entity_property_info().
 */
function comments_entity_entity_property_info() {
  $info = array();

  // Describe properties of entity.
  $properties = &$info['comments_entity']['properties'];

  $properties['id'] = array(
    'type' => 'integer',
    'schema field' => 'comment_id',
    'label' => t('ID'),
    'description' => t('Comment ID'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['type'] = array(
    'type' => 'bundle',
    'schema field' => 'type',
    'required' => TRUE,
    'label' => t('Type'),
    'description' => t('The bundle of comments entity.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['entity_id'] = array(
    'type' => 'integer',
    'schema field' => 'entity_id',
    'required' => TRUE,
    'label' => t('Entity id'),
    'description' => t('The unique entity id.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['entity_type'] = array(
    'type' => 'text',
    'schema field' => 'entity_type',
    'required' => TRUE,
    'label' => t('Entity type'),
    'description' => t('The entity type.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['origin_entity_id'] = array(
    'type' => 'integer',
    'schema field' => 'origin_entity_id',
    'label' => t('Origin entity id'),
    'description' => t('The unique entity id where the comment originated.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['origin_entity_type'] = array(
    'type' => 'text',
    'schema field' => 'origin_entity_type',
    'label' => t('Origin entity type.'),
    'description' => t('Entity type where the comment originated..'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['language'] = array(
    'type' => 'text',
    'schema field' => 'language',
    'label' => t('Language'),
    'description' => t('The language of the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['depth'] = array(
    'type' => 'integer',
    'schema field' => 'depth',
    'label' => t('Depth'),
    'description' => t('The depth of the comment in the tree.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['author_entity_id'] = array(
    'type' => 'integer',
    'schema field' => 'author_entity_id',
    'required' => FALSE,
    'label' => t('Author entity id'),
    'description' => t('The unique author entity id.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['author_entity_type'] = array(
    'type' => 'text',
    'schema field' => 'author_entity_type',
    'required' => FALSE,
    'label' => t('Author entity type'),
    'description' => t('The author entity type.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['hostname'] = array(
    'type' => 'text',
    'schema field' => 'hostname',
    'required' => FALSE,
    'label' => t('hostname'),
    'description' => t('Hostname of the author.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['is_deleted'] = array(
    'type' => 'boolean',
    'schema field' => 'is_deleted',
    'required' => FALSE,
    'label' => t('Deleted'),
    'description' => t('A boolean indicating if the comment is deleted.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['deleted_by_uid'] = array(
    'type' => 'integer',
    'schema field' => 'deleted_by_uid',
    'required' => FALSE,
    'label' => t('Deleted by User ID'),
    'description' => t('The user id who deleted the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['deleted_by_user'] = array(
    'type' => 'user',
    'schema field' => 'deleted_by_uid',
    'required' => FALSE,
    'label' => t('Deleted by User'),
    'description' => t('The user who deleted the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['is_approved'] = array(
    'type' => 'boolean',
    'schema field' => 'is_approved',
    'required' => FALSE,
    'label' => t('Approved'),
    'description' => t('A boolean indicating if the comment is approved.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['approved_by_uid'] = array(
    'type' => 'integer',
    'schema field' => 'approved_by_uid',
    'required' => FALSE,
    'label' => t('Approved by User ID'),
    'description' => t('The user id who approved the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['approved_by_user'] = array(
    'type' => 'user',
    'schema field' => 'approved_by_uid',
    'required' => FALSE,
    'label' => t('Approved by User'),
    'description' => t('The user who approved the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['is_internal'] = array(
    'type' => 'boolean',
    'schema field' => 'is_internal',
    'required' => FALSE,
    'label' => t('Internal'),
    'description' => t('A boolean indicating if the comment is internal.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['status'] = array(
    'type' => 'boolean',
    'schema field' => 'status',
    'required' => FALSE,
    'label' => t('Status'),
    'description' => t('Status of the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['created'] = array(
    'type' => 'date',
    'schema field' => 'created',
    'required' => FALSE,
    'label' => t('Created'),
    'description' => t('Created date of the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['changed'] = array(
    'type' => 'date',
    'schema field' => 'changed',
    'required' => FALSE,
    'label' => t('Changed'),
    'description' => t('Changed of the comment.'),
    'setter callback' => 'entity_property_verbatim_set',
  );

  return $info;
}
