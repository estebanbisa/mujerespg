/**
 * @file
 *
 * Implement a comments entity
 *
 * This javascript relies on jQuery Update, higher version than core.
 */

(function ($) {

    Drupal.behaviors.comments_entityLink = {
        attach: function(context, settings) {
            $(document).on("click", '.comments_entity-link', function(event) {
                event.preventDefault();
                var $this = $(this);

                var entity_type = $this.attr("data-entity-type");
                var entity_id = $this.attr("data-entity-id");
                var reply_id = $this.attr("data-reply-id");
                var thread_id = $this.attr("data-thread-id");
                var comments_entity_type = $this.attr('data-comments-entity-type');

                var wrapper_id = '#comments_entity-' + comments_entity_type + '-wrapper-' + entity_type + '-' + entity_id;

                var $wrapper_form = $(wrapper_id + '-form .comments_entity-submit-form');
                if($wrapper_form.length > 0) {
                    $(wrapper_id).toggle();
                }
                else {
                    Drupal.behaviors.comments_entityFormLoad.attach({
                        comments_entity_type: comments_entity_type,
                        entity_type: entity_type,
                        entity_id: entity_id,
                        reply_id: reply_id,
                        thread_id: thread_id
                    });
                }

            });
        }
    };

    Drupal.behaviors.comments_entityFormLoad = {
        attach: function(context, settings) {
            console.log(context);
            var entity_type = context.entity_type;
            var entity_id = context.entity_id;
            var reply_id = context.reply_id;
            var thread_id = context.thread_id;
            var comments_entity_type = context.comments_entity_type;

            if(entity_type && entity_id && comments_entity_type) {

                var path = "/comments_entity/form/" + comments_entity_type + "/"
                    + entity_type + "/" + entity_id + "/" + reply_id + "/" + thread_id;

                var wrapper_id = "#comments_entity-" + comments_entity_type +
                    "-wrapper-" + entity_type + '-' + entity_id;

                $.get( path, function( data ) {
                    if(data.success) {
                        $(wrapper_id + '-form').html( data.content );
                    }
                    else {
                        alert(data.content);
                    }
                });
            }

        }
    };

    Drupal.behaviors.comments_entitySubmit = {
        attach: function(context, settings) {
            $(document).on("keydown", '#edit-comments-entity-body-und-0-value', function(event) {
                if(event.keyCode == 13 && !event.shiftKey) {
                    var $this = $(this);
                    var $form = $this.parents('form');
                    $this.attr('readonly', 'readonly');
                    event.preventDefault();
                    $form.once('comment-submit').submit();
                }
            });
        }
    };

    Drupal.behaviors.commentFormSubmit = {
        attach: function(context, settings) {
            $(document).on("submit", '.comments_entity-submit-form', function( event ) {
                event.preventDefault();
                var $this = $(this);
                var action = $this.attr("action");

                $.ajax({
                    type: "POST",
                    url: action,
                    data: $this.serializeArray(), // serializes the form's elements.
                    success: function(data)
                    {
                        var wrapper = '#comments_entity-' +
                            data.comments_entity_type + '-wrapper-' +
                            data.entity_type + "-" + data.entity_id;
                        if(data.success) {
                            var $comments_entity_wrapper = $(wrapper + '-comments_entity');
                            var comments_entity_content_wrapper = wrapper + '-comments_entity .view-content';
                            $comments_entity_wrapper.addClass('well');
                            $(comments_entity_content_wrapper + ':not(' + comments_entity_content_wrapper + ' .view-content)').prepend( data.content );

                            Drupal.behaviors.comments_entityFormLoad.attach({
                                comments_entity_type: data.comments_entity_type,
                                entity_type: data.entity_type,
                                entity_id: data.entity_id,
                                reply_id: data.reply_id,
                                thread_id: data.thread_id
                            });

                        }
                        else {
                            alert('Something went wrong while submitting your comment. Please try again later.');
                            $(wrapper + ' #edit-comments-entity-body-und-0-value').removeAttr("readonly").val('');
                        }
                    }
                });

                event.preventDefault();
            });
        }
    };

    Drupal.behaviors.comments_entityLoad = {
        attach: function(context, settings) {

            var display = context.display;
            var entity_type = context.entity_type;
            var entity_id = context.entity_id;

            if(display && entity_id && entity_type) {
                var url = '/comments_entity/load/' + display + '/' + entity_type + '/' + entity_id;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data)
                    {
                        if(data.success) {
                            var selector = '.comments_entity-load[data-entity-type=' + entity_type + '][data-entity-id=' + entity_id + ']';
                            var $comments_entity_load = $(selector);
                            $comments_entity_load.html(data.content);
                        }
                        else {
                            console.log('Error loading comments_entity');
                        }
                    }
                });

            }

        }

    };

})(jQuery);
